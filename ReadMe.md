# node-red-broadcast-mask-generator


timewheel for broad casting on modbus where topic contains the broadcast mask

## Purpose

A interval timer that fires at multiples of the given interval, measured from Jan 1 1970.
This is especially useful for writing values to InfluxDB. Set the precision to "s" (seconds), then InfluxDB can use run-length-encoding for timestamps and saves a significant amout of space.

## Installation

```
npm install git+ssh://git@gitlab.com:gioxa/loging/node-red-broadcast-mask-generator.git#master
```

### License
MIT


### credits


learned from https://github.com/JsBergbau/node-red-contrib-interval-multiples-timer/blob/master/interval-multiples-timer.html

